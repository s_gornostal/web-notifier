#!/bin/bash

# add the following line to crontab
# 0 * * * * /bin/bash /home/aleksandr/projects/web-notifier/cron.sh

cd /home/aleksandr/.virtualenvs/web-notifier
source bin/activate

# virtualenv is now active, which means your PATH has been modified.  Don't try to run python from /usr/bin/python, just run "python" and let the PATH figure out which version to run (based on what your virtualenv has configured)
cd /home/aleksandr/projects/web-notifier
python run.py
