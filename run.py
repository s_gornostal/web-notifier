#!/usr/bin/env python
import gevent
from gevent.monkey import patch_all
patch_all()

import os
import logging

import config
from webnotifier import generate_rss

logging.basicConfig(format=' %(asctime)s (%(levelname)s) %(message)s')

if not os.path.exists(config.rss_dir):
    os.makedirs(config.rss_dir)


def run_scraper(scraper):
    try:
        mod = __import__('scrapers.{}'.format(scraper['name']), fromlist=['scrape'])
        entries = mod.scrape()
        feed = generate_rss(scraper, entries)
        with open(os.path.sep.join((config.rss_dir, scraper['name'])), 'w') as f:
            f.write(feed)
    except Exception as e:
        logging.error('{}: {}'.format(e.__class__.__name__, e), exc_info=True)

gevent.joinall([gevent.spawn(run_scraper, s) for s in config.scrapers])
