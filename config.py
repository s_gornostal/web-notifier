import os

project_dir = os.path.dirname(os.path.abspath(__file__))

rss_dir = '/home/aleksandr/Dropbox/Public/feeds'

scrapers = [
    dict(name='tbbt_eztv', title='The Big Bang Theory [EZTV]', link='http://eztv.it/shows/23/the-big-bang-theory/'),
    dict(name='south_park_eztv', title='South Park [EZTV]', link='http://eztv.it/shows/257/south-park/'),
    dict(name='family_guy_eztv', title='Family Guy [EZTV]', link='http://eztv.it/shows/92/family-guy/'),
    dict(name='sherlock_eztv', title='Sherlock [EZTV]', link='http://eztv.it/shows/376/sherlock/'),
    dict(name='suits_eztv', title='Suits [EZTV]', link='http://eztv.it/shows/495/suits/'),
    dict(name='forever_eztv', title='Forever [EZTV]', link='https://eztv.it/shows/1094/forever-us-2014/'),
    dict(name='underthedome_eztv', title='Under The Dome [EZTV]', link='http://eztv.it/shows/859/under-the-dome/'),
    dict(name='butterfly', title='kino-butterfly.com.ua', link='http://kino-butterfly.com.ua/cinema.php?sTheater=bilshovyk'),
    # dict(name='rutracker_trance', title='Rutracker trance', link='http://rutracker.org/forum/viewforum.php?f=1824')
    # dict(name='github_python', title='Gihub: Python', link='https://github.com/languages/Python')
]
