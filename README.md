# How to use this:

1. Add scraper description to the config file to the `scrapers` list

```python
scrapers = [
    dict(name='sample', title='My scraper', link='http://www.website.com/')
]
```

2. Create a scraper file in `scrapers/` directory
Sample file:

```python
import lxml.html
import requests


def scrape():
    try:
        # don't use lxml.html.parse() because gevent won't run in asynchronously
        resp = requests.get('http://www.website.com/posts')
        resp.raise_for_status()
    except requests.exceptions.HTTPError:
        return []

    page = lxml.html.fromstring(resp.text)
    elements = page.xpath("id('posts')//td[@class='post_item']")
    return map(get_entry, elements)


def get_entry(element):
    a = element.xpath('.//a[@class="title"]')[0]
    title = a.text_content().rstrip()
    link = 'http://www.website.com/{}'.format(a.attrib['href'])
    guid = link + title
    description = lxml.html.tostring(element)
    return dict(guid=guid, title=title, link=link, description=description)

```