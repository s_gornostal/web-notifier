import datetime
from werkzeug.contrib.atom import AtomFeed


def generate_rss(scraper, entries):
    """Generate string with feed items

    Args:
        scraper - dictionary with properties: name(required), title, link, description
        entries - list of dictionaries with props: title, description, link, guid
    """

    feed = AtomFeed(scraper.get('title', scraper['name']),
                    url=scraper.get('link'), subtitle=scraper.get('description'))
    for e in entries:
        try:
            feed.add(e['title'], e['description'], content_type='html', url=e['link'], id=e['guid'],
                     updated=datetime.datetime.now(), author=e.get('author'))
        except KeyError as e:
            raise KeyError('{}: Missing attribute {}'.format(scraper['name'], e))
    return str(feed)
