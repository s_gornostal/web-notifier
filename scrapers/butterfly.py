import lxml.html
import logging
import requests
from bs4 import BeautifulSoup


def scrape():
    try:
        resp = requests.get('http://kino-butterfly.com.ua/cinema.php?sTheater=bilshovyk')
        resp.raise_for_status()
    except requests.exceptions.HTTPError:
        logging.warning('butterfly loading error')
        return []

    page = lxml.html.fromstring(resp.text)
    elements = page.xpath("id('id_schedule')//a[@class='iframef']")
    return map(get_entry, filter_duplicates(elements))


def filter_duplicates(entries):
    seen = set()
    return [e for e in entries if e.attrib['href'] not in seen and not seen.add(e.attrib['href'])]


def get_entry(a):
    title = a.text_content().rstrip()
    link = a.attrib['href']
    guid = link
    description = title
    entry = dict(guid=guid, title=title, link=link, description=description)

    try:
        resp = requests.get('http://kino-butterfly.com.ua{}'.format(link))
        resp.raise_for_status()
    except requests.exceptions.HTTPError:
        logging.warning('butterfly loading error')
        return entry

    soup = BeautifulSoup(resp.text)
    entry['description'] = soup.body.div

    return entry
