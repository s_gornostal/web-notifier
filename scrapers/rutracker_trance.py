import lxml.html
import logging
import requests


def scrape():
    try:
        resp = requests.get('http://rutracker.org/forum/viewforum.php?f=1824')
        resp.raise_for_status()
    except requests.exceptions.HTTPError:
        logging.warning('rutracker loading error')
        return []

    page = lxml.html.fromstring(resp.text)
    elements = page.xpath("//a[contains(@href, 'viewtopic') and contains(@class, 'topictitle')]")
    return map(get_entry, filter(filter_links, elements))


def filter_links(element):
    """We want only specific topics"""
    title = element.text_content().rstrip().lower()
    allowed = ('Armin Van Buuren', 'A State of Trance', 'album')
    return any(a.lower() in title for a in allowed)


def get_entry(element):
    title = element.text_content().rstrip()
    link = element.attrib['href']
    guid = link
    description = title
    entry = dict(guid=guid, title=title, link=link, description=description)

    try:
        resp = requests.get('http://rutracker.org/forum{}'.format(link[1:]))
        resp.raise_for_status()
    except requests.exceptions.HTTPError:
        logging.warning('rutracker loading error')
        return entry

    page = lxml.html.fromstring(resp.text)

    # remove guest=1 from download links
    post = page.xpath("//div[contains(@class, 'post_body')]")[0]
    for e in post.xpath("//a[contains(@href, 'dl.rutracker.org/forum/dl.php')]"):
        e.attrib['href'] = e.attrib['href'].replace('guest=1', '')

    # remove unwanted text
    for e in post.xpath("id('guest-dl-tip')"):
        e.drop_tree()
    for e in post.xpath("//p[@class='bold tCenter mrg_8']") or []:
        e.drop_tree()

    entry['description'] = lxml.html.tostring(post)

    return entry
