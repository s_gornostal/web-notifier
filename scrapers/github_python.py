import lxml.html
import logging
import requests
from bs4 import BeautifulSoup


def scrape():
    try:
        resp = requests.get('https://github.com/languages/Python')
        resp.raise_for_status()
    except requests.exceptions.HTTPError:
        logging.warning('github loading error')
        return []
    html = resp.text.encode('utf8', 'ignore')
    page = lxml.html.fromstring(html)
    elements = page.xpath("//h3[text()='Most Starred This Week' or text()='Most Forked This Week']/following-sibling::ul/li")
    return map(get_entry, filter_duplicates(elements))


def filter_duplicates(entries):
    seen = set()
    return [e for e in entries if e.text_content() not in seen and not seen.add(e.text_content())]


def get_entry(element):
    title = element.text_content()
    (author, repo) = element.xpath('a')[:2]
    link = repo.attrib['href']
    guid = link
    author_name = author.text_content()
    description = title
    entry = dict(guid=guid, title=title, link=link,
                 description=description, author=author_name)

    try:
        resp = requests.get('https://github.com{}'.format(link))
        resp.raise_for_status()
    except requests.exceptions.HTTPError:
        logging.warning('github loading error')
        return entry

    soup = BeautifulSoup(resp.text)

    try:
        entry['description'] = soup.select('#readme')[0]
    except:
        entry['description'] = title

    return entry
