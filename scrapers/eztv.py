import lxml.html
import requests
import logging
from itertools import islice


def scrape(url):
    try:
        resp = requests.get(url)
        resp.raise_for_status()
    except requests.exceptions.HTTPError:
        logging.warning('eztv loading error')
        return []

    page = lxml.html.fromstring(resp.text)
    elements = page.xpath('//tr[@class="forum_header_border" and td[@class="forum_thread_post"]]')
    return map(get_entry, islice(elements, 20))


def get_entry(element):
    a = element.xpath('.//a[@class="epinfo"]')[0]
    title = a.text_content()
    link = 'http://eztv.it{}'.format(a.attrib['href'])
    guid = link
    description = a.attrib['title']
    return dict(guid=guid, title=title, link=link, description=description)
